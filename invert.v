module invert( 
                    clock,// master clock
					pinR,
					pinG,
					pinB,  // pixel in, synchronous with the clock
					poutR,
					poutG,
					poutB,  // pixel out
					 control
				);
				
// Define the image size:	
		
parameter SIZE_X = 768,
         SIZE_Y = 512; 

input clock, control;
input  [7:0] pinR;
input   [7:0] pinG;
input  [7:0] pinB;
output reg  [7:0] poutR;
output reg  [7:0] poutG;
output reg [7:0] poutB;		


// intermediate registers to compute the abs of H and V:
reg [11:0] H1, V1;

// iteration variable
integer i;
		  

always@(posedge clock) begin
    if (control)
	begin
	 	poutR <= 255-pinR;
		poutG <= 255-pinG;
		poutB <= 255-pinB;
	end
    else	
	begin
     		poutR <= 0;
		poutG <= 0;
		poutB <= 0;
	end
end

endmodule