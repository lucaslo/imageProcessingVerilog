`timescale 1ns / 1ns

module tCompact();
	parameter image_size = 768*512*3,header_size=54, image_width= 768, image_height=512;
	reg clock, control;
	reg  [7:0] pinR;
	reg  [7:0] pinG;
	reg  [7:0] pinB;
	wire [7:0] poutCompacted;
	wire [7:0] poutR;
	wire [7:0] poutG;
	wire [7:0] poutB;
	logic [32:0] iRamIn;
	logic [32:0] iRamOut;
	int fd;
	logic [7:0] RAM[(image_size*3-1):0]; //
	logic [7:0] RAM_out[(image_size-1)+header_size:0]; //

	integer file;
    
	
	compact dut (clock, pinR,pinG,pinB,poutR,poutG,poutB,pCompacted,control);
	reg  [54:0][7:0] BMP_header;
	
	function [54:0][7:0] headerMaker();
  		
    		// -- FILE HEADER -- //

   		 // headerMaker signature
   		 headerMaker[0] = 66;
   		 headerMaker[1] = 77;

   		 // file size
   		 headerMaker[5:2] = image_size*1/3+54; // 635+54
    		

   		 // reserved field (in hex. 00 00 00 00)
   		 for(int i = 6; i < 10; i++) headerMaker[i] = 0;

   		 // offset of pixel data inside the image
   		 headerMaker[10] = 54;

   		 // -- headerMaker HEADER -- //

   		 // header sizedo
   		 headerMaker[14] = 40;
   		 for(int i = 15; i < 18; i++) headerMaker[i] = 0;
	
   		 // width of the image
   		 headerMaker[21:18] = image_width;
   		 

   		 // height of the image
   		 headerMaker[25:22] = image_height;
   		

   		 // reserved field
   		 headerMaker[26] = 1;
   		 headerMaker[27] = 0;

   		 // number of bits per pixel
   		 headerMaker[28] = 8; 
   		 headerMaker[29] = 0;

   		 // compression method (no compression here)
   		 for(int i = 30; i < 34; i++) headerMaker[i] = 0;

   		 // size of pixel data
   		 headerMaker[34] = 8; // 12 bits => 4 pixels
   		 headerMaker[35] = 0;
   		 headerMaker[36] = 0;
   		 headerMaker[37] = 0;

   		 // horizontal resolution of the image - pixels per meter (2835)
   		 headerMaker[38] = 0;
   		 headerMaker[39] = 0;
   		 headerMaker[40] = 0'b00110000;
   		 headerMaker[41] = 0'b10110001;

   		 // vertical resolution of the image - pixels per meter (2835)
   		 headerMaker[42] = 0;
   		 headerMaker[43] = 0;
   		 headerMaker[44] = 48;
   		 headerMaker[45] = 177;

   		 // color pallette information
   		 for(int i = 46; i < 50; i++) headerMaker[i] = 0;

   		 // number of important colors
   		 for(int i = 50; i < 54; i++) headerMaker[i] = 0;

   		
	endfunction



	
	initial 
	begin
	
		clock = 0;
		
		pinR = 10'b0;
		pinG = 10'b0;
		pinB = 10'b0;

		
		iRamOut = header_size;
		
		$readmemh("monarchNovo.hex", RAM);
		$monitor("poutCompacted:%d,---------------------------------------------",poutCompacted); 
	     	file = $fopen ("monarchCompacted.bmp", "wb+");
		$monitor("pinR:%b poutR:%b  pinG:%b poutG:%b pinB:%b poutB:%b, POUTCOMPACTED:%b   ",pinR,poutR,pinG,poutG,pinB,poutB,poutCompacted); 
		BMP_header = headerMaker();
		control = 1;
		for(iRamIn=0; iRamIn<54; iRamIn=iRamIn+1) 
		begin
           	 	$fwrite(file, "%c", BMP_header[iRamIn][7:0]); // write the header
       		 end
	 	iRamIn = 0;
	end
	
	always
	begin
		#1 clock = !clock;
	end
	always @(posedge clock)
	begin
		if(control)
			begin
				if(iRamIn < (image_size-1+header_size) )
					begin
						pinR <= {2'b0, RAM[iRamIn]};
						pinG <= {2'b0, RAM[iRamIn+1]};
						pinB <= {2'b0, RAM[iRamIn+2]};
						fwrite(file, "%c", poutCompacted);
						
					end
				else
					begin
						control=0;
						$fclose(file);
						$stop;
				end
		end
	end
	
	always @(negedge clock) 
   	begin
		if(iRamOut<image_size-1+header_size)
			begin
				iRamIn <= iRamIn + 3;
				iRamOut <= iRamOut + 1;
	
			end
	end
	
	
	
endmodule