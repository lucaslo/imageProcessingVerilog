

module compact( 
                    clock,// master clock
			pinR,pinG,pinB,poutR,poutG,poutB,poutCompacted,control);
				
// Define the image size:	
		


function [2:0] most3(input [7:0] val8);
  most3 = val8[7:5];
endfunction
function [1:0] most2(input [7:0] val8);
  most2 = val8[7:6];
endfunction


input clock; 
input control;
input  [7:0] pinR;
input  [7:0] pinG;
input [7:0] pinB;
output reg [7:0] poutCompacted;		
output reg [2:0] poutR;		
output reg [2:0] poutG;
output reg [1:0] poutB;

// intermediate registers to compute the abs of H and V:


// iteration variable
integer i;
//constants to compact the byte
real redCompact ;
real blueCompact;
real greenCompact;

initial
	begin
		redCompact = 0.21;
		blueCompact = 0.07;
		greenCompact = 0.72;
		
	end
		  

always@(posedge clock) begin
    if (control)
	begin
		poutR =  most3(pinR);
		poutG =  most3(pinG);
		poutB =  most2(pinB);
		poutCompacted = {poutR,poutG,poutB};
	end
end

endmodule