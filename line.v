module line( 
                    clock,// master clock
					pinR,
					pinG,
					pinB,  // pixel in, synchronous with the clock
					poutR,
					poutG,
					poutB,  // pixel out
					 control
				);
				
// Define the image size:	
		
parameter image_size = 768*512*3,header_size=54, image_width= 768, image_height=512, frontier = 50.0;
parameter inclination = -1, b =  image_height;


input clock, control;
input  [7:0] pinR;
input   [7:0] pinG;
input  [7:0] pinB;
output reg  [7:0] poutR;
output reg  [7:0] poutG;
output reg [7:0] poutB;		
reg [15:0]posX;
reg [15:0]posY;
// intermediate registers to compute the abs of H and V:
real firstTerm, secondTerm;
reg inLine;
reg [15:0]centerX;
reg [15:0]centerY;
real distance;

// iteration variable
integer i;
		  
initial
	begin
		
		posX=0;
		posY=0;
		centerX = image_width * 0.5;
		centerY = image_height * 0.5;
	end
always@(posedge clock) begin
	
	firstTerm =  posY;
	
	if(inclination<0)
		begin
			if(b<0)
				begin
					secondTerm = posX*(inclination*-1)+(b*-1);
					inLine = (firstTerm+secondTerm)<frontier;
				end
			else
				begin
					if (b > posX*(inclination*-1))
						begin
							secondTerm = b -posX*(inclination*-1);
							inLine = (firstTerm>secondTerm)?((firstTerm-secondTerm)<frontier):((secondTerm-firstTerm)<frontier);
						end
					else
						begin
							secondTerm = posX*(inclination*-1)-b;  
							inLine = (firstTerm+secondTerm)<frontier;
						end
				end
			
				
			
		end
	else
		begin
			secondTerm= posX*inclination+b;
			inLine = (firstTerm>secondTerm)?((firstTerm-secondTerm)<frontier):((secondTerm-firstTerm)<frontier);
			
		end	
    if (control)
	begin
		
		if(posX == image_width-1)
			begin
				posY <= posY+1;
				posX <= 0 ;
				poutR <=pinR;
				poutG <=pinG;
				poutB <=pinB;
				
				
			end
		
		else
			begin
				
				if(inLine)
					begin
						poutR <=255-pinR;	
						poutG <=255-pinG;
						poutB <=255-pinB;	
						posX<=posX+1;			
					end
				else
					begin				
						poutR <=pinR;
						poutG <=pinG;
						poutB <=pinB;
						posX<=posX+1;
					end				
			
		end
		
	end
    else
	begin
     		poutR <= 0;
		poutG <= 0;
		poutB <= 0;
	end
end

endmodule