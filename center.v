module center( 
                    clock,// master clock
					pinR,
					pinG,
					pinB,  // pixel in, synchronous with the clock
					poutR,
					poutG,
					poutB,  // pixel out
					 control
				);
				
// Define the image size:	
		
parameter image_size = 768*512*3,header_size=54, image_width= 768, image_height=512, frontier = 100.0;

input clock, control;
input  [7:0] pinR;
input   [7:0] pinG;
input  [7:0] pinB;
output reg  [7:0] poutR;
output reg  [7:0] poutG;
output reg [7:0] poutB;		
reg [15:0]posX;
reg [15:0]posY;
// intermediate registers to compute the abs of H and V:
real firstTerm, secondTerm;

reg [15:0]centerX;
reg [15:0]centerY;
real distance;

// iteration variable
integer i;
		  
initial
	begin
		
		posX=0;
		posY=0;
		centerX = image_width * 0.5;
		centerY = image_height * 0.5;
	end
always@(posedge clock) begin
	
	firstTerm =  (centerX>posX)?(centerX-posX):(posX-centerX);
	secondTerm = (centerY>posY)?(centerY-posY):(posY-centerY);
	firstTerm = firstTerm**2;
	secondTerm= secondTerm**2;
  	 distance = (firstTerm+secondTerm)**(0.5);
	
    if (control)
	begin
		
		if(posX == image_width-1)
			begin
				posY <= posY+1;
				posX <= 0 ;
				poutR <=pinR;
				poutG <=pinG;
				poutB <=pinB;
				
				
			end
		
		else
			begin
				
				if(distance < frontier)
					begin
						poutR <=255-pinR;	
						poutG <=255-pinG;
						poutB <=255-pinB;	
						posX<=posX+1;			
					end
				else
					begin				
						poutR <=pinR;
						poutG <=pinG;
						poutB <=pinB;
						posX<=posX+1;
					end				
			
		end
		
	end
    else
	begin
     		poutR <= 0;
		poutG <= 0;
		poutB <= 0;
	end
end

endmodule