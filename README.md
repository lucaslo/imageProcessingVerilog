# imageProcessingVerilog


Different Modules for Image Processing in Hardware using Verilog

    line.v - Draws a line given an a and a b (ax+b)
    center.v - Draws a Circle given the coordinates of the center and the radius size.
    border.v - Draws a border given the thickness of it
    invert.v - INvert the colors of an image
    grayscale.v - Convert an image to grayscale mode 

